<?php
namespace App\Http\Controllers;
 
use App\Schedule;
use App\Event;
use App\Ticket;
use App\Transaction;
use App\Invoice;
use App\Attend;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
 
class TransactionController extends Controller {
 
   
    public function index_invoice()
    {
        $result=DB::table('invoice')
            ->select('invoice.id','invoice.id_attend','attend.id_identity','attend.name','attend.email','event.event_name',
            DB::raw('sum(if(invoice.id=transaction.id_invoice,transaction.qty_ticket,0)) as total_ticket'),
            DB::raw('sum(if(invoice.id=transaction.id_invoice,transaction.qty_ticket*transaction.price,0)) as total_payment'))
            ->join('transaction','invoice.id','=','transaction.id_invoice')
            ->join('attend','invoice.id_attend','=','attend.id')
            ->join('ticket','transaction.id_ticket','=','ticket.id')
            ->join('event','ticket.id_event','=','event.id')
            ->groupBy('invoice.id')
            ->get();
 
        return response()->json($result);
    }

  
    public function index_invoice_detail($id)
    {
        $result=DB::table('invoice')
            ->select('invoice.id','attend.id_attend','attend.id_identity','attend.name','attend.email','event.event_name','invoice.total_ticket','invoice.total_payment')
            ->join('transaction','invoice.id','=','transaction.id_invoice')
            ->join('attend','attend.id_invoice','=','invoice.id')
            ->join('ticket','transaction.id_ticket','=','ticket.id')
            ->join('event','ticket.id_event','=','event.id')
            ->where('invoice.id','=', $id)
            ->get();
 
        $result2=DB::table('invoice')
            ->select('ticket.id','ticket.type','ticket.price')
            ->join('transaction','invoice.id','=','transaction.id_invoice')
            ->join('attend','attend.id_invoice','=','invoice.id')
            ->join('ticket','transaction.id_ticket','=','ticket.id')
            ->join('event','ticket.id_event','=','event.id')
            ->where('invoice.id','=', $id)
            ->get();

            $result[0]->tickets = $result2;

            $response = [
                'status'    => true,
                'result'    => $result[0]
            ];
           
            return response()->json($response);
    }


    public function purchase(Request $request)
    {  
        $this->validate($request,[
            'id_identity'   =>'numeric',
            'name'          =>'required|max:255',
            'email'         =>'required|max:255',
            'telp'          =>'numeric',
            'gender'        =>'required',
            'dob'           =>'date_format:"Y-m-d"|required',
            'id_event'      =>'required'

        ]);

        $id           = $request->input('id');
        $id_identity  = $request->input('id_identity');
        $name         = $request->input('name');
        $email        = $request->input('email');
        $telp         = $request->input('telp');
        $gender       = $request->input('gender');
        $dob          = $request->input('dob');
        $id_ticket    = $request->input('id_ticket');
        $qty_ticket   = $request->input('qty_ticket');
        $id_event     = $request->input('id_event');
        $id_invoice   = $request->input('id_invoice');
       
        //data tiket
        $ticket    = Ticket::find($id_ticket);
        $total_payment = $qty_ticket * $ticket->price;
     
        //create invoice
        $data1 = new \App\Invoice();
        $data1->id_event        = $id_event;
        $data1->total_ticket    = $qty_ticket;
        $data1->total_payment   = $total_payment;

        $qty = $ticket->qty;

        if ($qty <1)
        {
            return response()->json(['status' => 'error', 'message' => 'Ticket is SoldOut'],404); 
        }
            if ($qty< $qty_ticket)
            {
                return response()->json(['status' => 'error', 'message' => 'Ticket is not enough'],404); 
            }
            
        $data1->save();
        $id_invoice = $data1->id;

        //create attend
        $data2  = new \App\Attend();

        $data2->id_identity     = $id_identity;
        $data2->name            = $name;
        $data2->email           = $email;
        $data2->telp            = $telp;
        $data2->gender          = $gender;
        $data2->dob             = $dob;
        $data2->id_invoice      = $id_invoice;
      
        //create transaction

        $data3 = new \App\Transaction();

        $data3->id_invoice      = $id_invoice;
        $data3->id_ticket       = $id_ticket;
        $data3->qty_ticket      = $qty_ticket;
        

        if(($data1->save()) && ($data2->save()) && ($data3->save())){
            
            $result=DB::table('attend')
            ->select('invoice.id','attend.id_identity','attend.name','attend.email','attend.telp','attend.gender','attend.dob',
            'transaction.id_ticket','transaction.qty_ticket')
            ->join('invoice','attend.id_invoice','=','invoice.id')
            ->join('transaction','transaction.id_invoice','=','invoice.id')
            ->where('invoice.id','=', $id_invoice)
            ->get();

            return response()->json($result, 200);
        }
    }
 
}