<?php
namespace App\Http\Controllers;
 
use App\Event;
use App\Location;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
 
class EventController extends Controller {
 
   

    public function getdetail()
    {   
        $result=DB::table('event')
        ->select('event.id','event.event_name','event.description','location.name_location','location.address',
        'schedule.start','schedule.end')
        ->join('location','event.id_location','=','location.id')
        ->join('schedule','schedule.id_event','=','event.id')
        ->groupBy('event.id')
        ->get();

        return response()->json(['status' => 'success', 'data' => $result]);
    }

    public function store(Request $request)
    {  
        $this->validate($request,[
            'event_name'    =>'required',
            'description'   =>'required',
            'id_location'   =>'required',
            'start'         =>'date_format:"Y-m-d H:i:s"|required',
            'end'           =>'date_format:"Y-m-d H:i:s"|required',
        ]);

        $id            = $request->input('id');
        $event_name    = $request->input('event_name');
        $description   = $request->input('description');
        $id_location   = $request->input('id_location');
        $start         = $request->input('start');
        $end           = $request->input('end');

        //create event
        $data1 = new \App\Event();
        
        $data1->id           = $id;
        $data1->event_name   = $event_name;
        $data1->description  = $description;
        $data1->id_location  = $id_location;

        $location = Location::find($id_location);

        if($location == null)
        {
            return response()->json(['status' => 'error', 'message' => 'ID Location not found'],404);
        } 
        $data1->save();
        
        //create schedule
        $data2 = new \App\Schedule();

        $id_event           = $data1->id;

        $data2->id_event    = $id_event;
        $data2->start       = $start;
        $data2->end         = $end;
 
        if($data1->save() && $data2->save())
        {
            $result=DB::table('event')
            ->select('event.id','event.event_name','event.description','location.name_location','location.address',
            'schedule.start','schedule.end')
            ->join('location','event.id_location','=','location.id')
            ->join('schedule','schedule.id_event','=','event.id')
            ->where('event.id','=', $id_event)
            ->get();

            $response = [
                'status'    => true,
                'result'    => $result[0],
            ];
                 return response()->json($response);
            }  
        return response()->json(['status' => 'error', 'message' => 'Failed to save'],404);
    }
 
    public function show($id)
    {
        $result=DB::table('event')
        ->select('event.id','event.event_name','event.description','location.name_location','location.address',
        'schedule.start','schedule.end')
        ->join('location','event.id_location','=','location.id')
        ->join('schedule','schedule.id_event','=','event.id')
        ->where('event.id','=', $id)
        ->get();

        $result2= DB::table('ticket')
        ->select(DB::raw('id,type,price,qty'))
        ->where('id_event','=', $id)
        ->get();

        $result[0]->tickets = $result2;

        $response = [
            'status'    => true,
            'result'    => $result[0]
        ];
       
        return response()->json($response);
    } 
}