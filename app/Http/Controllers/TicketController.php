<?php
namespace App\Http\Controllers;
 
use App\Schedule;
use App\Event;
use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
class TicketController extends Controller {
 
    public function index()
    {
        $ticket = Ticket::all();
        $response = [
            'status'    => true,
            'result'    => $ticket,
        ];
        return response()->json($response);
    }
 
    public function store(Request $request)
    {  
        $this->validate($request,[
            'id_event'    =>'numeric',
            'id_schedule' =>'numeric',
            'type'        =>'required|max:255',
            'price'       =>'numeric',
            'qty'         =>'numeric',
        ]);

        $id         = $request->input('id');
        $id_event   = $request->input('id_event');
        $id_schedule= $request->input('id_schedule');
        $type       = $request->input('type');
        $price      = $request->input('price');
        $qty        = $request->input('qty');

        $data = new \App\Ticket();
        
        $data->id           = $id;
        $data->id_event     = $id_event;
        $data->id_schedule  = $id_schedule;
        $data->type         = $type;
        $data->price        = $price;
        $data->qty          = $qty;

        $event    = Event::find($id_event);
        $schedule = Schedule::find($id_schedule);

        if($event ==null)
        {
            return response()->json(['status' => 'error', 'message' => 'ID Event not found'],404);
        }
            if($schedule ==null)
             {
                return response()->json(['status' => 'error', 'message' => 'ID Schedule not found'],404);
             } 
                if($data->save())
                {
                    $response = [
                        'status'    => true,
                        'result'    => $data,
                    ];
                    return response()->json($response,200);
                }
    } 
 
    public function show($id)
    {
        $ticket = Ticket::find($id);
        $response = [
            'status'    => true,
            'result'    => $ticket,
        ];
        return response()->json($response);
    }

    public function showticket($id)
    {
        $result= DB::table('ticket')
         ->select(DB::raw('id,id_schedule,type,price,qty'))
         ->where('id_event','=', $id)
         ->get();

         $response = [
            'status'    => true,
            'result'    => $result,
        ];
        return response()->json($response,200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'id_event'    =>'numeric',
            'id_schedule' =>'numeric',
            'type'        =>'required|max:255',
            'price'       =>'numeric',
            'qty'         =>'numeric',
        ]);

        $id_event   = $request->input('id_event');
        $id_schedule= $request->input('id_schedule');
        $type       = $request->input('type');
        $price      = $request->input('price');
        $qty        = $request->input('qty');

        $data = new \App\Ticket();

        $data->id           = $id;
        $data->id_event     = $id_event;
        $data->id_schedule  = $id_schedule;
        $data->type         = $type;
        $data->price        = $price;
        $data->qty          = $qty;
        
        $ticket   = Ticket::find($id);
        $schedule = Schedule::find($id_schedule);
        $event    = Event::find($id_event);

        if($ticket ==Null)
        {
            return response()->json(['status' => 'error', 'message' => 'ID Ticket is not found'],404); 
        }   
            if($event ==Null)
            {
                return response()->json(['status' => 'error', 'message' => 'ID Event is not found'],404); 
            }
                if($schedule ==Null)
                {
                    return response()->json(['status' => 'error', 'message' => 'ID Schedule is not found'],404); 
                }   
            
                    $ticket->update($request->all());
                    
                    $response = [
                        'status'    => true,
                        'result'    => $ticket,
                    ];
                    return response()->json($response,200);          
     }
 
    public function delete($id)
    {
        Ticket::destroy($id);
 
        return response()->json([
        'message' => 'Successfull delete Ticket'
        ]);
    }
}