<?php
namespace App\Http\Controllers;
 
use App\Location;
use Illuminate\Http\Request;
 
class LocationController extends Controller {
 
    public function index()
    {
        $location = Location::all();
        $response = [
            'status'    => true,
            'result'    => $location,
        ];
        return response()->json($response);
    }
 
    public function store(Request $request)
    {  
        $this->validate($request,[
            'name_location' =>'required|max:255',
            'address'       =>'required|max:255',
        ]);

          $location = Location::create($request->all());

          $response = [
            'status'    => true,
            'result'    => $location,
           ];
            return response()->json($response,201);
    }
 
    public function show($id)
    {
        $location = Location::find($id);

        $response = [
            'status'    => true,
            'result'    => $location,
        ];
        return response()->json($response);
    }
 
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name_location' =>'required|max:255',
            'address'       =>'required|max:255',
        ]);

        $location = Location::find($id);
        $location->update($request->all());

        $response = [
            'status'    => true,
            'result'    => $location,
        ];
        return response()->json($response,200);
    }
 
    public function delete($id)
    {
        Location::destroy($id);
 
        return response()->json([
        'message' => 'Successfull delete Location'
        ]);
    }
 
 
}