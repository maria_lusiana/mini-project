<?php
namespace App\Http\Controllers;
 
use App\Event;
use App\Attend;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
 
class AttendController extends Controller {
 
    public function index()
    {   
        $attend = Attend::all();
        return response()->json($attend);
   
    }
    public function getdetail()
    {   
        $result=DB::table('event')
        ->select('event.id','event.event_name','event.description','location.name_location','location.address',
        'schedule.start','schedule.end')
        ->join('location','event.id_location','=','location.id')
        ->join('schedule','schedule.id_event','=','event.id')
        ->groupBy('event.id')
        ->get();

        return response()->json(['status' => 'success', 'data' => $result]);
    }

    public function store(Request $request)
    {  
        $this->validate($request,[
            'event_name'    =>'required',
            'description'   =>'required',
            'id_location'  =>'required',
        ]);
        $id            = $request->input('id');
        $event_name    = $request->input('event_name');
        $description   = $request->input('description');
        $id_location   = $request->input('id_location');
    
        $data = new \App\Event();
        
        $data->id           = $id;
        $data->event_name   = $event_name;
        $data->description  = $description;
        $data->id_location  = $id_location;

        $location = Location::find($id_location);

        if($location)
        {
            if($data->save())
            {
                $res['message'] = "Success!";
                $res['value']   = "$data";        
                return response()->json($data, 200);
            }
                return response()->json(['status' => 'error', 'message' => 'Failed to save'],404);
        }   
            return response()->json(['status' => 'error', 'message' => 'ID Location not found'],404);
    }
 
    public function show($id)
    {
        $result=DB::table('event')
        ->select('event.id','event.event_name','event.description','location.name_location','location.address',
        'schedule.start','schedule.end')
        ->join('location','event.id_location','=','location.id')
        ->join('schedule','schedule.id_event','=','event.id')
        ->where('event.id','=', $id)
        ->get();

        $result2=DB::table('event')
        ->select('ticket.id','ticket.type','ticket.price','ticket.qty')
        ->join('ticket','event.id','=','ticket.id_event')
        ->where('event.id','=', $id)
        ->get();

        $result=DB;
        
        foreach($result as $event){
            foreach($result2 as $ticket);
            echo $ticket;
        }
        


       // $event = Event::find($id);
       // return response()->json($result);  
    }
 
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'event_name'    =>'required',
            'description'   =>'required',
            'id_location'   =>'required',
        ]);

        $event_name   = $request->input('event_name');
        $description  = $request->input('description');
        $id_location  = $request->input('id_location');
    
        $data = new \App\Event();
        
        $data->id          = $id;
        $data->event_name  = $event_name;
        $data->description = $description;
        $data->id_location = $id_location;

        $location = Location::find($id_location);
        $event    = Event::find($id);
        
        if($event ==!null)
        {
             if($location ==!null)
             {
                $event->update($request->all());
                return response()->json($event,200);
            }
            return response()->json(['status' => 'error', 'message' => 'ID Location is not Found'],404);
        }
        return response()->json(['status' => 'error', 'message' => 'ID Event is not Found'],404);
    }
 
    public function delete($id)
    {
        Event::destroy($id);
 
        return response()->json([
        'message' => 'Successfull delete Event'
        ]);
    }
 
 
}