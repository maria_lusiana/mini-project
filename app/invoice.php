<?php
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Invoice extends Model {
 
    protected $fillable = ['id_attend','id_event','id_location','qty_ticket','total_payment'];
    public $table       = "invoice";
    
    public static $rules = [
        'id_attend'      => 'required',
        'id_event'       => 'required',
        'id_location'    => 'required',
        'qty_location'   => 'required',
        'qty_ticket'     => 'required',
        'total_payment'  => 'required',
    ]; 

 }