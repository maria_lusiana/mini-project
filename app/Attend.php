<?php
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Attend extends Model {
 
    protected $fillable = ['id_identity','name','email','telp','gender','dob'];
    public $table       = "attend";
    
    public static $rules = [
        'id_identity'  => 'required',
        'name'         => 'required',
        'email'        => 'required',
        'telp'         => 'required',
        'gender'       => 'required',
        'dob'          => 'required',
    ]; 

 }