<?php
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Location extends Model {
 
    protected $fillable = ['name_location','address','id_event','id_schedule'];
    protected $dates    = [];
    public $table       = "location";
 
    public static $rules = [
        'name_location' => 'required',
        'address'       => 'required',
        ];

    public function getEvent()
    {
        return $this->hasMany('App\Event','location','id');
    }
    
 }