<?php
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Event extends Model {
 
    protected $fillable = ['event_name','description','id_location'];
    public $table       = "event";
    
    public static $rules = [
        'event_name'    => 'required',
        'description'   => 'required',
        'id_location'   => 'requiered',
    ]; 

    public function getLocation()
    {
        return $this->belongsTo('App\Location','location','id');
    }
    
    public function getSchedule()
    {
        return $this->hasOne('App\Schedule');
    }

 }