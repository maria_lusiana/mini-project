<?php
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Ticket extends Model {
 
    protected $fillable = ['id_event','id_schedule','type','price','qty'];
    protected $dates    = [];
    public $table       = "ticket";
    
    public static $rules = [
        'id_event'      => 'required',
        'id_schedule'   => 'required',
        'type'          => 'required',
        'price'         => 'required',
        'qty'           => 'required',
    ]; 

    public function get_transaction(){
    	return $this->hasMany('App\transaction');
    }
 }