<?php
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Transaction extends Model {
 
    protected $fillable = ['id_ticket','qty_ticket','price'];
    protected $dates    = [];
    public $table       = "transaction";
    
    public static $rules = [
        'id_ticket'      => 'required',
        'qty_ticket'     => 'required',
        'price'          => 'required',
    ]; 

    public function get_ticket(){
        return $this->belongsTo('App\ticket','id_ticket');
    }

 }